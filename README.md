# Kwartz-Client #
Dans les Hauts de France, la plupart des établissements scolaires disposant
d'un réseau local pédagogique utilisent un service 
[Kwartz](https://www.kwartz.com/fr/nos-produits/kwartz-server) qui
supporte divers services dont :

*  un annuaire LDAP, qui gère les utilisateurs et le groupes, ainsi que les données réseau des ordinateurs clients ;
*  des partages SAMBA, pour des données personnelles aux utilisateurs, ou partagées par des groupes d'utilisateurs ;
*  un service Owncloud, et un service DAV, qui offrent des opportunités de synchronisation entre fichiers locaux et fichiers partagés ;
*  ... et d'autres services encore.

Le paquet Debian ``kwartz-client`` facilite l'intégration de tout 
client GNU-Linux dans ce type de réseau. Il suffit de répondre à 
quelques questions pour assurer une configuration du client prête
à l'emploi. Ces questions sont posées lors de la première installation
du paquet ``kwartz-client`` ; pour modifier les réponses aux questions
on peut lancer la commande ``sudo dpkg-reconfigure kwartz-client``

Après configuration, la commande ``sudo kwartz-install.sh`` permet
d'intégrer proprement la machine cliente dans le réseau Kwartz local.



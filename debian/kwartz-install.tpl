#! /usr/bin/sh

#############################################
# Run using sudo, of course.
#############################################
if [ "$(id -u)" -ne "0" ]
then
   echo "Il faut etre root pour executer ce script. ==> sudo "
   exit
else
   echo "
+------------------------------------------------+
| ATTENTION, il est préférable que l'utilisateur |
| 'user' ait bel et bien défini un mot de passe. |
+------------------------------------------------+
| Après avoir exécuté le présent script, un      |
| accès multi-utlisateurs sera facilité.         |
+------------------------------------------------+
"
fi 


# extinction programmée de la machine cliente
if [ "$proghalt" = 1 ] ; then
    echo "0 19 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
else
    if [ "$proghalt" = 2 ] ; then
        echo "0 20 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
    else
	if [ "$proghalt" = 3 ] ; then
            echo "0 22 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
	fi
    fi
fi
# paramètres du service de proxy
echo "Paramétrage du proxy $ip_proxy:$port_proxy"
# Pour Gnome
echo "[org.gnome.system.proxy]
mode='manual'
use-same-proxy=true
ignore-hosts=$proxy_gnome_noproxy
[org.gnome.system.proxy.http]
host='$ip_proxy'
port=$port_proxy
[org.gnome.system.proxy.https]
host='$ip_proxy'
port=$port_proxy
" >> /usr/share/glib-2.0/schemas/my-defaults.gschema.override

  glib-compile-schemas /usr/share/glib-2.0/schemas

# Pour le système
echo "http_proxy=http://$ip_proxy:$port_proxy/
https_proxy=http://$ip_proxy:$port_proxy/
ftp_proxy=http://$ip_proxy:$port_proxy/" >> /etc/profile

# Pour APT
if [ "$unattended_upgrades" = "true" -a "$login_apt" != "nobody" ]; then
   echo "Acquire::http::proxy \"http://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";
Acquire::ftp::proxy \"ftp://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";
Acquire::https::proxy \"https://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";" > /etc/apt/apt.conf.d/10proxy
fi

#Permettre d'utiliser la commande add-apt-repository derrière un Proxy
echo "Defaults env_keep += http_proxy" > /etc/sudoers.d/01-environment
echo "Defaults env_keep += https_proxy" >> /etc/sudoers.d/01-environment
echo "Defaults env_keep += ftp_proxy" >> /etc/sudoers.d/01-environment

# Modification pour ne pas avoir de problème lors du rafraichissement des dépots avec un proxy
# cette ligne peut être commentée/ignorée si vous n'utilisez pas de proxy ou avec la 14.04.
if ! grep -q "Acquire::http::No-Cache" /etc/apt/apt.conf; then
    echo "Acquire::http::No-Cache true;" >> /etc/apt/apt.conf
fi
if ! grep -q "Acquire::http::Pipeline-Depth"  /etc/apt/apt.conf; then
    echo "Acquire::http::Pipeline-Depth 0;" >> /etc/apt/apt.conf
fi

####################################################################
# Activation du module PAM pam_mkhomedir.so
####################################################################
if ! grep -q "pam_mkhomedir.so" /etc/pam.d/common-session; then
    sed -i "/pam_unix.so/ a session required        pam_mkhomedir.so skel=/etc/skel umask=0022" /etc/pam.d/common-session
fi

######################################################################
#protocole pour SMB
######################################################################
if grep -q "^client min protocol" /etc/samba/smb.conf; then
    # il y a déjà un protocole ; on le met à jour
    sed -i 's/client min protocol.*/client min protocol = NT1/' /etc/samba/smb.conf
else
    # il n'y a pas encore de protocole ; on le crée en fin de fichier
    echo "client min protocol = NT1" >> /etc/samba/smb.conf
fi

######################################################################
#Montage auto des partages du serveur Kwartz
######################################################################
# montage du partage Perso
if grep -q "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Espace_Perso\"" /etc/security/pam_mount.conf.xml; then
    echo "Perso déjà présent"
else
    sed -i "/<\!-- Volume definitions -->/ a <volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"%(USER)\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Espace_Perso\" options=\"vers=1.0\" />" /etc/security/pam_mount.conf.xml
fi

# montage du partage Commun
if grep -q "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Commun\"" /etc/security/pam_mount.conf.xml; then
    echo "Commun déjà présent"
else
    sed -i "/<\!-- Volume definitions -->/ a <volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"Commun\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Commun\" options=\"vers=1.0\" />" /etc/security/pam_mount.conf.xml
fi

# montage du partage Public
if grep -q "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Public\"" /etc/security/pam_mount.conf.xml; then
    echo "Public déjà présent"
else
    sed -i "/<\!-- Volume definitions -->/ a <volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"Public\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Public\" options=\"vers=1.0\" />" /etc/security/pam_mount.conf.xml
fi

######################################################################
# activation auto des mises à jour de sécurité
######################################################################
if [ "unattended_upgrade" = "true" ]; then
    echo "APT::Periodic::Update-Package-Lists \"1\";
APT::Periodic::Unattended-Upgrade \"1\";" > /etc/apt/apt.conf.d/20auto-upgrades
fi

######################################################################
# langue par défault, pour /etc/profile
######################################################################
if ! grep -q "LC_ALL=" /etc/profile; then
   echo "
export LC_ALL=fr_FR.utf8
export LANG=fr_FR.utf8
export LANGUAGE=fr_FR.utf8
" >> /etc/profile
fi
   

######################################################################
#ne pas créer les dossiers par défaut dans home
######################################################################

sed -i "s/enabled=True/enabled=False/g" /etc/xdg/user-dirs.conf

######################################################################
# les profs peuvent sudo
########################################################################

if grep -q "%professeurs ALL=(ALL) ALL" /etc/sudoers; then
    echo "prof déjà dans sudo"
else
    sed -i "/%admin ALL=(ALL) ALL/a\%professeurs ALL=(ALL) ALL" /etc/sudoers
    sed -i "/%admin ALL=(ALL) ALL/a\%DomainAdmins ALL=(ALL) ALL" /etc/sudoers
fi

######################################################################
#Activation du shell 
######################################################################

ln -sf /bin/bash /bin/kwartz-sh

######################################################################
#FIN
######################################################################
echo "C'est fini ! Un reboot est nécessaire..."
echo -n "Voulez-vous redémarrer immédiatement ? [O/n] "
read rep_reboot

if [ "${DOLLAR}rep_reboot" = "O" ] || [ "${DOLLAR}rep_reboot" = "o" ] || [ "${DOLLAR}rep_reboot" = "" ] ; then
  reboot
fi
